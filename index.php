<!DOCTYPE html>
<html> 
    <head>
        <meta charset="UTF-8">
        <title>Südfruchtpedia</title>
        <link rel="icon" type="image/png" href="/favicon.png"/>

        <!-- Stylesheets -->
        <link rel="stylesheet" href="styles/css/style.css">
        <link rel="stylesheet" href="styles/css/header.css">

        <!-- Scripts -->
        <script src="/src/scripts/header-icon.js"></script>
        <script src="/src/scripts/memeform.js"></script>
    </head>
    <body>
        <div class="main-frame">
            <?php include "src/components/memeheader.php"?>
        </div>
    </body>
</html>